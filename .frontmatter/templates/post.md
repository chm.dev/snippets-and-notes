---
title: How to serve Cockpit admin interface via Caddy reverse proxy
summary: Cockpit needs special treatment to be served via Caddy server reverse proxy
date: 2020-10-05T00:00:00.000Z
tags:
  - linux
  - caddy
  - webserver
  - cockpit
  - reverse-proxy
---

