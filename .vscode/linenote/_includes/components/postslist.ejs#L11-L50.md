for (const tag of post.data.tags) {
if (tag != "posts") {
if (typeof firstTag == 'undefined')
firstTag = tag;
tagsHTML += `<a href="/tags/${tag}" class="tag is-black">${tag}</a>`;
}
} %>

<% //make a proper readable date
const postDate = new Date( post.date );
let dateString = `${postDate.getDate()} ${ ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ][postDate.getMonth()]} ${postDate.getFullYear()}`;
%>

<div class="card post-item">
  <div class="card-content">
    <article class="media">
      <div class="media-left">
        <figure class="image tag-icon <%- firstTag %>">
        </figure>
      </div>
      <div class="media-content">
        <h1><a href="<%- post.url %>"> <% if(post.data.title) { %>
            <%- post.data.title %><% } else { %><code><%- post.url %></code>
            <% } %></a></h1>
        <%- post.data.summary %>
      </div>
    </article>
  </div>
  <footer class="card-footer">
    <div class="card-footer-item">
      <time class="postlist-date" datetime="<% post.date %>">
        <%- dateString %>
      </time></div>
    <div class="card-footer-item tags"><%- tagsHTML %></div>
  </footer>
</div>
