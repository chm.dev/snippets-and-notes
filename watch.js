const chokidar = require('chokidar');
const { execSync } = require('child_process');

const callback = (error, stdout, stderr) => {
  if (error) {
    console.error(`exec error: ${error}`);
    return;
  }
  console.log(stdout || stderr);
};

chokidar
  .watch(
    [
      '**/*.css',
      '**/*.scss',
      '**/*.njk',
      '**/*.md',
      '**/*.ejs',
      '**/static*.*',
      '**/_includes*.*',
      '**/assets*.*'
    ],
    { ignored: /(?:_site|node_modules|\.git|theme\.css)/gi }
  )
  .on('change', path => {
    console.log('File changed: ' + path);
    if (path.includes('assets\\scss\\')) {
      console.log('Building Bulma !');
      console.log(
        execSync(
          'npx node-sass ./_includes/assets/scss/ -o ./_site/static/css/ --source-map tru' +
            'e --source-map-contents true',
          { stdio: 'pipe' }
        ).toString()
      );
      console.log(execSync('npm run scss', { stdio: 'pipe' }).toString());
    } else {
      console.log(execSync('npx eleventy ', { stdio: 'pipe' }).toString());
    }
  });
