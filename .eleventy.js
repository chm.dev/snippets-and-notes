const { DateTime } = require( 'luxon' );
const CleanCSS = require( 'clean-css' );
const UglifyJS = require( 'uglify-js' );
const htmlmin = require( 'html-minifier' );
const syntaxHighlight = require( '@11ty/eleventy-plugin-syntaxhighlight' );
const pluginRss = require( '@11ty/eleventy-plugin-rss' );
const pluginNavigation = require( '@11ty/eleventy-navigation' );
const markdownIt = require( 'markdown-it' );
const ejs = require( 'ejs' );

module.exports = function( eleventyConfig ) {
  eleventyConfig.addPlugin( pluginRss );
  eleventyConfig.addPlugin( pluginNavigation );
  eleventyConfig.addPlugin( syntaxHighlight );
  eleventyConfig.addLayoutAlias( 'post', 'layouts/post.njk' );
  eleventyConfig.addLayoutAlias( 'feed', 'feed/feed.njk' );
  eleventyConfig.addLayoutAlias( 'admin', '/admin/' );


  // Date formatting (human readable)
  eleventyConfig.addFilter( 'readableDate', dateObj => {
    return DateTime.fromJSDate( dateObj ).toFormat( 'dd LLL yy' );
  } );

  // https://html.spec.whatwg.org/multipage/common-microsyntaxes.html#valid-date-string
  eleventyConfig.addFilter( 'htmlDateString', dateObj => {
    return DateTime.fromJSDate( dateObj, {
      zone: 'utc'
    } ).toFormat( 'yyyy-LL-dd' );
  } );

  // Date formatting (machine readable)
  eleventyConfig.addFilter( 'machineDate', dateObj => {
    return DateTime.fromJSDate( dateObj ).toFormat( 'yyyy-MM-dd' );
  } );
  // Get the first `n` elements of a collection.
  eleventyConfig.addFilter( 'head', ( array, n ) => {
    if ( n < 0 ) {
      return array.slice( n );
    }

    return array.slice( 0, n );
  } );

  eleventyConfig.addFilter( 'json', function JSONstringify( obj ) {
    function objectToSafeStrings( toStringify ) {
      if ( Array.isArray( toStringify ) ) {
        return toStringify.map( function( o ) {
          return objectToSafeStrings( o );
        } );
      }
      else if ( _.isObject( toStringify ) ) {
        return _.mapValues( toStringify, value => {
          if ( _.isObject( value ) ) {
            return objectToSafeStrings( value );
          }
          else if ( _.isString( value ) ) {
            return value.replace( /"/g, '\\"' );
          }
          return value;
        } );
      }
      return toStringify;
    }
    return JSON.stringify( objectToSafeStrings( obj ) );
  } );
  // Minify CSS
  eleventyConfig.addFilter( 'cssmin', function( code ) {
    return new CleanCSS( {} ).minify( code ).styles;
  } );

  // Minify JS
  eleventyConfig.addFilter( 'jsmin', function( code ) {
    let minified = UglifyJS.minify( code );
    if ( minified.error ) {
      console.log( 'UglifyJS error: ', minified.error );
      return code;
    }
    return minified.code;
  } );

  // Minify HTML output
  eleventyConfig.addTransform( 'htmlmin', function( content, outputPath ) {
    if ( outputPath.indexOf( '.html' ) > -1 ) {
      let minified = htmlmin.minify( content, {
        useShortDoctype: true,
        removeComments: true,
        collapseWhitespace: true
      } );
      return minified;
    }
    return content;
  } );

  // only content in the `posts/` directory
  eleventyConfig.addCollection( 'posts', function( collection ) {
    return collection
      .getAllSorted()
      .reverse()
      .filter( function( item ) {
        return item.inputPath.match( /^\.\/posts\// ) !== null;
      } );
  } );

  // only content in the latest `posts/` directory
  eleventyConfig.addCollection( 'postsLatest', function( collection ) {
    return collection
      .getFilteredByGlob( '**/posts/*.md' )
      .slice( -9 )
      .reverse();
  } );

  eleventyConfig.addCollection( 'tagList', require( './_11ty/getTagList' ) );

  // Don't process folders with static assets e.g. images
  eleventyConfig.addPassthroughCopy( 'static/' );
  eleventyConfig.addPassthroughCopy( 'admin/' );

  /* Markdown Plugins */
  let markdownLibrary = markdownIt( {
    html: true,
    breaks: true,
    linkify: true
  } );

  eleventyConfig.setLibrary( 'ejs', ejs );

  eleventyConfig.setLibrary( 'md', markdownLibrary );
  return {
    templateFormats: ['md', 'njk', 'html', 'ejs'],

    // If your site lives in a different subdirectory, change this. Leading or
    // trailing slashes are all normalized away, so don’t worry about it. If you
    // don’t have a subdirectory, use "" or "/" (they do the same thing) This is
    // only used for URLs (it does not affect your file structure)
    pathPrefix: '/',

    markdownTemplateEngine: 'liquid',
    htmlTemplateEngine: 'ejs',
    dataTemplateEngine: 'ejs',
    passthroughFileCopy: true,
    dir: {
      input: '.',
      includes: '_includes',
      data: '_data',
      output: '_site'
    }
  };
};
