---
title: "Enabling starship prompt in Nushell "
date: 2020-10-22T11:16:19.788Z
summary: "After updating nushell via scoop starship gets dropped for some reason. "
tags:
  - nushell
  - nu
  - prompt
  - commandline
  - sh
---
If you installed [nushell](https://www.nushell.sh/) on windows via scoop and are using starship prompt you might noticed that starship prompt gets dropped after `scoop update nu` comeback to bare prompt without starship anymore. 

To get it back simply paste this snippet (taken directly from nushell docs)

```shell
config set prompt `echo $(starship prompt)`
```

**Update**: new versions of nu changed the way prompt config value is handled. Starship is now external, so you need it to install it manually (ie `scoop install starship`). Once its installed set it as default prompt with

```shell
config set prompt "starship prompt"
```