---
title: 'Toggle window snippet (show / hide / bring to front) with Autohotkey'
date: 2020-03-04T02:11:02.559Z
summary: >-
  This piece of code toggles any window. It will 
  * RESTORE if window is minimized,
  * Bring to the top so window is foremost if it is behind other windows,
  * Minimize if window is active and on top,
tags:
  - autohotkey
  - ui
  - windows
---
# Quick toggle any window. 

This piece of code toggles any window [Autohotkey]. 

It will 
  * RESTORE if window is minimized
  * Bring to the top so window is foremost if it is behind other windows
  * MINIMIZE if window is currently active and on top

Spotify is a being toggled in the below code, just as an example. 


```autohotkey
WindowTitle = ahk_exe Spotify.exe ;just as an example 

if (ID := WinExist(WindowTitle)){
    WinGet, state, MinMax, %WindowTitle%
    
    if (state = -1) {
         WinRestore %WindowTitle%
         WinActivate
    } Else {
         WinGetPos , X, Y, , , %WindowTitle%
         If  ( WinActive(WindowTitle))
         WinMinimize, %WindowTitle%
         Else
         WinActivate, %WindowTitle%
    } 
}
```