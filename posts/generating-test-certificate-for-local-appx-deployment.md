---
title: Generating test certificate for local appx deployment
date: 2020-06-12T16:35:01.665Z
summary: Short PowerShell snippet to generate local test certificate for Windows
  UWP apps development.
tags:
  - wsl
  - wsl2
  - appx
  - linux
  - pengwin
---
`As of Visual Studio 2019, the original feature to create Test Certificates have been removed in order to, "promote better security practices". A bug report has been filed, but it does not seem that this feature will be coming back soon. `

Taken from [Pengwin Build Instructions](https://github.com/WhitewaterFoundry/Pengwin/blob/master/BUILDING.md)

**[ISSUE SEEM TO BE FIXED BY NOW](https://developercommunity.visualstudio.com/comments/736713/view.html)** but this short powershell snippet to generate own test cert without using VS can be still quite handy:

```powershell
$Subject = "CN=Your-appxmanifest-publisher"
$Store = "Cert:\CurrentUser\My"

# Delete old certificate
Get-ChildItem $Store | Where-Object { $_.Subject -match $Subject } | Remove-Item

# Create new certificate
New-SelfSignedCertificate -Type Custom -Subject $Subject -KeyUsage DigitalSignature -FriendlyName "Pengwin Test Certificate" -CertStoreLocation $Store -TextExtension @("2.5.29.37={text}1.3.6.1.5.5.7.3.3", "2.5.29.19={text}")
```


