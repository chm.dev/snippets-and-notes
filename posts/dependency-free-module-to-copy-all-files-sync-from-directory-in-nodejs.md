---
title: Dependency free module to copy all files (sync) from directory in nodejs
date: 2019-12-18T17:52:52.950Z
summary: Simple synchronous copy all files in directory to destination path.
tags:
  - nodejs
  - javascript
  - fs
  - copy
---
```javascript
const fs = require( 'fs' );
const path = require( 'path' );

module.exports = ( source, destination ) => {
  if ( !fs.statSync( source ).isDirectory() ) throw 'Source path must be directory';
  if ( !fs.statSync( destination ).isDirectory() )
    throw 'Destination path must be directory';

  const files = fs.readdirSync( source );
  for ( var index in files ) {
    fs.copyFileSync(
      path.join( source, files[index] ),
      path.join( destination, files[index] )
    );
  }
};
```

You can always promisify fs.stat and copyFile if you want to be pro ;)
