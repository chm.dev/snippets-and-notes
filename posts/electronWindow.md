---
title: Electron window shenanigans
summary: Few methods showing how to build non-standard electron window. 
date: 2019-12-11
tags:
  - electron
  - javascript
  - ui
---

Electron is pretty cool. The way i use it lately is rather for building small and simple tools. I need my ui to be also small and smart.
Methods below are mostly copied from official electoron documentation, but it is worth to remeber that you can do stuff like this.

## Click-through window

To create a click-through window, i.e. making the window ignore all mouse events, you can call the [win.setIgnoreMouseEvents(ignore)](https://devdocs.io/electron/api/browser-window#winsetignoremouseeventsignore) API:

```javascript
const { BrowserWindow } = require("electron");
let win = new BrowserWindow();
win.setIgnoreMouseEvents(true);
```

### Forwarding

Ignoring mouse messages makes the web page oblivious to mouse movement, meaning that mouse movement events will not be emitted. On Windows operating systems an optional parameter can be used to forward mouse move messages to the web page, allowing events such as `mouseleave` to be emitted:

```javascript
let win = require("electron").remote.getCurrentWindow();
let el = document.getElementById("clickThroughElement");
el.addEventListener("mouseenter", () => {
  win.setIgnoreMouseEvents(true, { forward: true });
});
el.addEventListener("mouseleave", () => {
  win.setIgnoreMouseEvents(false);
});
```

This makes the web page click-through when over `el`, and returns to normal outside it.

## Transparent window

By setting the `transparent` option to `true`, you can also make the frameless window transparent:

```javascript
const { BrowserWindow } = require("electron");
let win = new BrowserWindow({ transparent: true, frame: false });
win.show();
```

Be aware though that this way you have no UI to move or minimize / maximize your window anymore. This is why you have to use below methods to make window usable again.

## Draggable region

By default, the frameless window is non-draggable. Apps need to specify `-webkit-app-region: drag` in CSS to tell Electron which regions are draggable (like the OS's standard title bar), and apps can also use `-webkit-app-region: no-drag` to exclude the non-draggable area from the draggable region. Note that only rectangular shapes are currently supported.

Note: `-webkit-app-region: drag` is known to have problems while the developer tools are open. See this [GitHub issue](https://github.com/electron/electron/issues/3647) for more information including a workaround.

To make the whole window draggable, you can add `-webkit-app-region: drag` as `body`'s style:

```html
<body style="-webkit-app-region: drag">
  </body>
```

And note that if you have made the whole window draggable, you must also mark buttons as non-draggable, otherwise it would be impossible for users to click on them:

```css
button {
  -webkit-app-region: no-drag;
}
```

If you're setting just a custom title bar as draggable, you also need to make all buttons in title bar non-draggable.

## Text selection

In a frameless window the dragging behavior may conflict with selecting text. For example, when you drag the title bar you may accidentally select the text on the title bar. To prevent this, you need to disable text selection within a draggable area like this:

```css
.titlebar {
  -webkit-user-select: none;
  -webkit-app-region: drag;
}
```
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTY2NzczNTU4OV19
-->