---
title: List all subfolders in current location and calculate their sizes
summary: Short snippet stored as alias on linux, listing all subfolders in current location and calculate its sizes. 
date: 2019-12-14
tags:
  - linux
  - bash
  - sh
  - fs
  - filesystem
---
So, there is probably one milion ways of achiving same result, but this is how i do it.

The command (or rather script) looks like so:

```bash
$ for each in $(echo */ | sed "s/ /\n/g" | sed "s/\///g");  do du -hs "$each"; done;
```

If you want to save it as alias permanently:
```bash
$ echo $'alias ds=\'for each in $(echo */ | sed "s/ /\\n/g" | sed "s/\\///g");  do du -hs "$each"; done;\''>>~/.bashrc
```

and if you don't want to wait for next shell login for it just source the .bashrc file:

```bash 
$ source ~/.bashrc
```

So now you are good to take it for a spin: 

```bash
$ chm@chm.dev 12:10:09 /home →  cd /var/www/ && ds
52M     alltube
47M     grav
8.0K    html
6.5M    leed
584M    mattermost
136K    mumbleserver
2.5M    rss-bridge
166M    wallabag2
chm@qchm.dev 12:10:40 /var/www →
```

Keep in mind that in this simple version of it you cannot pass path. It will always list current path. 
Of course it wouldn't take much of an effort to make it work like that, but for me way it works is fine. 

