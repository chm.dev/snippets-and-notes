---
title: Remove new lines in quotations from csv with Regex
date: 2020-04-01T15:25:07.089Z
summary: Regex Expression to remove new lines which are quoted in csv.
tags:
  - regex
  - csv
---
Csv is a weird format. It has never been officially documented by Microsoft and still there is no hard standards for any library to follow. Especially around quoting strings and new lines (outside of unquoted ones which mark new row of a datatable)

```regex
/(?<=\"[^\"]*)[\r\n]+(?=[^\"]*\"\,)/gi
```

It should work fine for both windows and unix new lines quoted between regular quotation mark ( " ).

If you want to get rid of new lines you can use regex regex replace with above expression, and replace with \t - which will be tab.

