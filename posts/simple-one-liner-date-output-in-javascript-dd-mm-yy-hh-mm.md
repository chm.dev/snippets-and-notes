---
title: 'Quick formatted {dd-mm-yy hh mm} date output in JavaScript '
date: 2019-12-18T16:11:02.559Z
summary: >-
  The simplest way I know to get formatted date in JavaScript in format
  {dd-mm-yy hh:mm}
tags:
  - javascript
  - date
  - time
---
No matter how many times I need do it I always end up in docs or stackoverflow.

And let's be honest date formatting in js is ... well [not that simple](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/toLocaleDateString).

This snippet is as straightforward as possible, but gets job done. I usually use it for folder naming or other minor stuff, where getting separate lib like [Moment.js](https://momentjs.com/) just for that seems like overkill.

```javascript
const t = new Date();
const formattedDate =
  t.getDate() +
  '-' +
  t.getMonth() +
  '-' +
  t.getFullYear().toString().replace(/^\d{2}/, '') +
  '\s' +
  t.getHours() +
  ':' +
  t.getMinutes(); // output '18-11-19 17:37'
```

Of course if you want other date than "now" you need to pass it in the Date constructor.

But if you are in need of more advanced, flexible and dependency-free ...

[`package.json`](https://github.com/date-fns/date-fns/blob/master/package.jsonhttps://github.com/date-fns/date-fns/blob/master/package.json)
```json
{
// ...
   "dependencies": {},
//..
}
```
library for handling time in js [date-fns](https://date-fns.org/https://date-fns.org/) should do just perfect.
