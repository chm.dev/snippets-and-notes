---
title: Programming STM32f1 using Raspberry pi 400 and openocd
description: ''
date: '2023-02-02T20:23:02.206Z'
preview: ''
draft: ''
tags:
  - linux
  - terminal
  - raspberry
  - rpi
lastmod: '2022-02-02T20:27:10.084Z'
slug: programming-stm32f1-using-raspberry-pi-400-and-openocd
---
Had to flash Gotek drive FDD-UDD U144K, which is STM32f1x based floppy drive emulator.
Motherboard is similar to most popular Gotek model but it has differnt header pinouts and has no ttl header.
Normaly you can program it using stlink flasher, but here is how to do it using only rasppbery pi.

Install openocd with apt (in my case 0.11.0-rc2 on debian) 

Create Interface cfg file
```ini
#Use RPi GPIO pins
interface bcm2835gpio

#Base address of I/O port - this is changes with different rpi models - use xxd -c 4 -g 4 /proc/device-tree/soc/ranges to check
bcm2835gpio_peripheral_base 0x00000000

#Clock scaling
bcm2835gpio_speed_coeffs 146203 36

#SWD                swclk swdio
# Header pin numbers 22    18
bcm2835gpio_swd_nums 25    24

#JTAG                tck tms tdi tdo
#Header pin numbers  22  18  16  15
bcm2835gpio_jtag_nums 25  24  23  22
```

Connect pins to your stm32 board In caser this particular Gotek it is as follows:
* Connect GND and +5V to power connector of gotek
* Connect SWDIO (rpi pin 24) to header pin 46 (PA13)
* Connect SWCLK (rpi pin 25) to header pin 49 (PA14)
* Connect header pin 07 (NRST) to GND 

Run (ocd_rpi.cfg is file defined above) 
```bash
openocd -f ./ocd_rpi.cfg -c "transport select swd" -f /usr/share/openocd/scripts/target/stm32f1x.cfg
```

In a different shell execute "telnet localhost 4444" (you might need to install it - use apt). 
From now on we use the telnet session.
Type "reset halt" and immediately disconnect header pin 07. The reply should be something like
target halted due to debug-request, current mode: Thread xPSR: 0x01000000 pc: 0x080061d4 msp: 0x200006c0

"stm32f1x unlock 0" was needed on this board to enable writing to the flash bank. ("flash banks" displays all available banks)
type
" flash write_image erase <your_path>/<file>.hex"
type "reset run"
disconnect st-link.

Original tutorial found here: [in Github issue](https://github.com/keirf/flashfloppy/issues/232). I just replaced stlink with raspberry pi.

Further reading for using openocd for stm32 programming [can be found in this great Blog post](https://iosoft.blog/2019/01/28/raspberry-pi-openocd/)
