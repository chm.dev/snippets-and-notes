---
title: Sorting Objects By Property With Compare Function In JS
description: ''
date: '2022-02-07T20:23:02.206Z'
preview: ''
draft: ''
tags:
  - javascript
  - sort
  - sorting
  - compareFn
lastmod: '2022-02-07T20:27:10.084Z'
slug: sorting-with-compare-function-js
---

Say you have array of objects ...

```javascript
const articles = [
  {
    id: 6,
    isRead: false,
    url: "https://phys.org/news/2021-04-jupiter-ideal-dark-detector.html",
    title: "Jupiter could make an ideal dark matter detector",
    summary:
      'So you want to find dark matter, but you don"t know where to look. A giant planet might be exactly ........',
    coverUrl: "https://scx2.b-cdn.net/gfx/news/2021/jupitercould.jpg",
    published_at: "2021-04-12T20:58:11.658Z",
    created_at: "2021-04-12T20:58:11.660Z",
    updated_at: "2022-01-17T14:12:50.986Z",
    cover: null
  },
  {
    id: 19,
    isRead: false,
    url:
      "https://news.cgtn.com/news/2021-04-17/Chinese-company-unveils-self-developed-CPU-architecture--ZvSw4KIJq0/index.html",
    title:
      '"Historic breakthrough": Chinese chipmaker unveils self-developed CPU architecture',
    summary:
      "Loongson Technology, a leading Chinese chip company based in Beijing, unveiled its fully self-developed CPU (central processing )",
    coverUrl:
      "https://news.cgtn.com/news/2021-04-17/Chinese-company-unveils-self-developed-CPU-architecture--ZvSw4KIJq0/img/ac3b76ef6ba44fd885" +
      "9a3a3879323f01/ac3b76ef6ba44fd8859a3a3879323f01-750.jpeg",
    created_at: "2021-04-19T10:35:33.254Z",
    updated_at: "2021-04-25T15:33:46.931Z",
    cover: null
  }
];
```

... and you want to sort it from the lastet to the oldest.
You need to use compare function calling [sort array method](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort) like so:

```javascript
articles.sort(function(a, b) {
  const d1 = new Date(a.updated_at);
  const d2 = new Date(b.updated_at);
  return d1 > d2 ? -1 : 1;
});
```
