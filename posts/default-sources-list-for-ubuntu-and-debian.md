---
title: Default sources.list for Ubuntu and Debian
date: 2020-06-17T19:57:48.980Z
summary: "In case you mess up sources.list here are default sources.list generators "
tags:
  - linux
  - ubuntu
  - debian
---
If things get out of hand and are beyond repair get default sources.list for your version of 

Ubuntu:

[https://repogen.simplylinux.ch/](https://repogen.simplylinux.ch/)

Debian:

[https://debgen.simplylinux.ch](https://debgen.simplylinux.ch/)