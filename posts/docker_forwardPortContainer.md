---
title: Expose additional port in running docker container
summary: Sometimes you need to expose additional port on running container without rebuilding it from image.
date: 2019-12-02
tags:
  - docker
  - bash
---

Sometimes you need to expose additional port on running container without rebuilding it from image.

Here are steps which worked for me: 

1. stop the container 
2. stop docker service (can skip - restart later is enough)
3. change the files

```bash
/var/lib/docker/containers/<conainerID>
```

```bash
config.v2.json
```

```json
"Config": {
    ....
    "ExposedPorts": {
        "80/tcp": {},
        "8888/tcp": {}
    },
    ....
},
"NetworkSettings": {
....
"Ports": {
     "80/tcp": [
         {
             "HostIp": "",
             "HostPort": "80"
         }
     ],
```

```bash
hostconfig.json
```

```json
"PortBindings": {
     "80/tcp": [
         {
             "HostIp": "",
             "HostPort": "80"
         }
     ],
     "8888/tcp": [
         {
             "HostIp": "",
             "HostPort": "8888"
         } 
     ]
 }
```



restart your docker engine (to flush/clear config caches) 

```bash
systemctl restart docker	
```

5) start the container

<!--stackedit_data:
eyJoaXN0b3J5IjpbLTEzMzQ2NjUxNjQsMTAwMDM2MzMxNCw5Nj
M3OTU3NThdfQ==
-->