---
title: MS SQL insert or update if record exists
date: 2020-07-01T14:11:53.953Z
summary: Simple and easy to modify sample for inserting new rows into MS SQL
  database or update in case of matching PRIMARY KEY.
tags:
  - SQL
  - MSSQL
  - MYSQL
---

## MS SQL:

```sql
if not exists (select 1 from employees where employee_id = 1)
      insert into employees (employee_id,last_name,first_name) values ( 1,'smith', 'bob' )
else
      update employees set last_name='smith' , first_name='bob' where employee_id =   
end 
```
Above snippet found here: 
https://www.withdata.com/blog/sql-server/replace-update-or-insert-a-row-into-sql-server-table.html



Same thing in 
## MySQL:
```sql
INSERT INTO table (id, name, age) VALUES(1, "A", 19) ON DUPLICATE KEY UPDATE    
name="A", age=19
```

