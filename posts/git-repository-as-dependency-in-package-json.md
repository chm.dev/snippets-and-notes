---
title: Git repository as dependency in package.json.
date: 2019-12-19T10:42:20.450Z
summary: How to add dependency straight from git rather then npm.
tags:
  - git
  - dependencies
---
Very simple, but easy to forget.  Instead of npm package name and version you provide URL to the git repository. 

In anchor of the URL you can put commit id to link specific version. You can provide short version.

`package.json` 

```json
{
  "name": "cool-named-project",
  //  ... 
  "dependencies": {
    "config": "^3.2.4",
    "slash": "^3.0.0",
    "dialog": "git+https://github.com/chm-dev/dialog.git#80c8f03"
  },
  //  ... 
}
```

After npm install git package is listed like so:

```shell
$ npm list

cool-named-project@0.0.1 C:\Dev\Projects\cool-named-project
+-- config@3.2.4
| `-- json5@1.0.1
|   `-- minimist@1.2.0
+-- dialog@0.3.1 (git+https://github.com/chm-dev/dialog.git#80c8f03b875c9fb1e6b1c2c3f8a0fbbcf03df978)
`-- slash@3.0.0
```
