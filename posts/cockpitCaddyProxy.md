---
title: How to serve Cockpit admin interface via Caddy reverse proxy
summary: Cockpit needs special treatment to be served via Caddy server reverse proxy
date: 2020-10-05
tags:
  - linux
  - caddy
  - webserver
  - cockpit
  - reverse-proxy
---

 



After installing [Cockpit](https://cockpit-project.org/) from packet manager (on Debian in this case) and trying to reverse-proxy it's default local port, as usual, you will notice that you can't log in. 



Create a file  /etc/cockpit/cockpit.conf

```ini
[WebService]
AllowUnencrypted = true
Origins = https://[DESTINATION HOST]
ProtocolHeader = X-Forwarded-Proto
```

Reload cockpit

```bash
systemctl restart cockpit.socket
```
 
