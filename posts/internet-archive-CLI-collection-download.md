---
title: Internet Archive CLI full collection download
description: ''
date: '2023-02-14T20:23:02.206Z'
preview: ''
draft: ''
tags:
  - CLI
  - terminal
  - ia
lastmod: '2022-02-14T20:27:10.084Z'
slug: internet-archive-cli-full-collection-download
---
1. Find the collection name. It's part of the url: https://archive.org/details/your_collection_name
2. Login with the CLI (account required)
```bash
ia configure
```
3. Get the files
```bash
ia download --search collection:your_collection_name
```



[Found on reddit](https://www.reddit.com/r/DataHoarder/comments/giaav9/comment/fqdphan/?utm_source=share&utm_medium=web2x&context=3)