---
title: Proper way to set display for external X server in wsl
date: 2020-04-17T20:26:53.770Z
summary: "Proper way to set display for external X server in wsl:"
tags:
  - wsl bash linux
---
Proper way to set display for external X server in wsl:

```bash
export DISPLAY=$(cat /etc/resolv.conf | grep nameserver | awk '{print $2}'):0
export LIBGL_ALWAYS_INDIRECT=1
```