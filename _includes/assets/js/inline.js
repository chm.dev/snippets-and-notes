document.addEventListener('DOMContentLoaded', function() {
  new Swup({
    linkSelector:
      'a[href^="' +
      window.location.origin +
      '"]:not([data-no-swup]), a[href^="/"]:no' +
      't([data-no-swup]), a[href^="./"]:not([data-no-swup]), a[href^="#"]:not([data-n' +
      'o-swup])',
    containers: ['#pagecontent', 'nav'],
    cache: true,
    animateHistoryBrowsing: false,
    plugins: [
      new SwupFadeTheme(),
      new SwupScrollPlugin({
        doScrollingRightAway: true,
        animateScroll: true,
        scrollFriction: 0.3,
        scrollAcceleration: 0.04
      }),
      new SwupPreloadPlugin()
    ]
  });
});
